<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Categories;
use app\models\Products;
use app\models\SortForm;
use app\models\Characteristics;
use yii\web\NotAcceptableHttpException;
/*Контроллер для страниц сайта*/
class PageController extends Controller
{
   

    /**
       Для страницы списка товаров
     */
    public function actionListproducts()
    {
        if(isset($_GET['ID']) && $_GET['ID']!="" && filter_var($_GET['ID'],FILTER_VALIDATE_INT))
        {
            //ID категории
            $ID = $_GET['ID'];


            $categories = Categories::find()->where(['ID'=> $ID])->asArray()->one();

            $characteristics = Characteristics::find()->where(['name'=> $IDprod])->asArray()->one();

            if(count($categories) > 0)
           {  
              $model = new SortForm();

              $count_products = count(Products::find()->where(['category' => $ID])->asArray()->all());

              $page = 1; // номер страницы
              $str = null; //сортировка
              $number = 12; //кол-во товаров на странице

              if(isset($_GET['page']) && $_GET['page']!="" && filter_var($_GET['page'],FILTER_VALIDATE_INT))
              {
                $page = $_GET['page'];
              }

              // обработчик для формы сортировки
              if ($model->load(Yii::$app->request->post()) && $model->validate())
              {

                if(isset($model->number) && !empty($model->number)){
                    $number = $model->number;
                }
           
                if(isset($model->str)){

                    switch ($model->str) {
                        case 0:
                            $products_array = $this->selectListProd($ID, ['price'=> SORT_ASC], $number, $page);
                           break;
                        case 1:
                             $products_array = $this->selectListProd($ID, ['price'=> SORT_DESC], $number, $page);
                           break;
                        case 2:
                            $products_array = $this->selectListProd($ID, ['name'=> SORT_ASC], $number, $page);
                           break;
                        case 3:
                            $products_array = $this->selectListProd($ID, ['name'=> SORT_DESC], $number, $page);
                           break;
                        default:
                            $products_array = $this->selectListProd($ID, ['ID'=> SORT_ASC], $number, $page);
                           break;

                    }
                }
                else{
                    $products_array = $this->selectListProd($ID, ['ID'=> SORT_ASC], $number, $page);
                }

             }

             else {
                $products_array = $this->selectListProd($ID, ['ID'=> SORT_ASC], $number, $page);
              }

              //кол-во страниц для пагинации
              $count_pages = ceil($count_products / $number);
              

              if(isset($_GET['view']) && $_GET['view'] == 1)
                $view = 1;
            else
                $view = 0;

              return $this->render('listproducts', compact('categories','products_array','count_products','view','model','count_pages','ID', 'characteristics'));
            }

        }
            return $this->redirect(['page/catalog']);

            
        }
       
        private function selectListProd($ID, $field_sort, $limit, $start){

            if($start == 1)
                $start = 0;
            else
                $start = ($start - 1) * $limit;

                return Products::find()->where(['category' => $ID])->asArray()->orderBy($field_sort)->limit($limit)->offset($start)->all();
    }

        /**
       Для страницы каталога
     */
    public function actionCatalog()
    {
 
        $categories = Categories::find()->asArray()->all();
        
        return $this->render('catalog',compact('categories'));
    }

            /**
       Для страницы каталога
     */
    public function actionProduct()
    {
 
       $this->layout = "product";
      
      if(isset($_GET['ID']) && !empty($_GET['ID']) && filter_var($_GET['ID'], FILTER_VALIDATE_INT)){
            $ID = $_GET['ID'];
        }
        else{
            throw new NotAcceptableHttpException;
        }

        $product_array = Products::find()->where(['ID' => $ID])->asArray()->one();
        if(!is_array($product_array) || count($product_array) < 0){
            throw new NotAcceptableHttpException;
        }


        return $this->render('product', compact('product_array', 'ID'));
    }


     /**
       Для страницы новостей
     */
    public function actionNews()
    {

        return $this->render('news');
    }

  /**
       Для страницы контакты
     */
    public function actionContacts()
    {

        return $this->render('contacts');
    }

  /**
       Для страницы войти
     */
    public function actionLogin()
    {

        return $this->render('login');
    }

/**
       Для страницы обратная связь
     */
    public function actionConnection()
    {

        return $this->render('connection');
    }
/**
       Для страницы личный кабинет
     */
    public function actionCabinet()
    {

        return $this->render('cabinet');
    }
/**
     	Для страницы Доставка
     */
    public function actionDostavka()
    {
        return $this->render('dostavka');
    }

    /**
     	Для страницы Оплата
     */
    public function actionOplata()
    {
        return $this->render('oplata');
    }

    /**
     	Для страницы О компании
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     	Для страницы Скидки
     */
    public function actionSale()
    {
        return $this->render('sale');
    }

    /**
     	Для страницы Карта сайта
     */
    public function actionSitemap()
    {
        return $this->render('sitemap');
    }

    /**
     	Для страницы корзина
     */
    public function actionCart()
    {
        return $this->render('cart');
    }

    /**
     	Для страницы Список желаний
     */
    public function actionListorder()
    {
        return $this->render('listorder');
    }

 /**
     	Для страницы зарегистрироваться
     */
    public function actionRegistration()
    {
        return $this->render('registration');
    }

}

