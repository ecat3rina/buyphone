      <?php

use yii\helpers\Url;

$this->title = 'Корзина';
?>

      <div class="row cart_wrap">
        <div class="col-lg-12 top_cart_block">
          <div>
            <p>Состояние корзины</p>
            <p>Ваша корзина содержит: 1 товар</p>
          </div>
        </div>
        <div class="col-lg-12">
          <ul class="cart_status">
            <li class="active"><span>1. Заказ</span></li>
            <li><span>2. Адрес</span></li>
            <li><span>3. Доставка</span></li>
            <li><span>4. Оплата</span></li>
          </ul>
        </div>
        <div class="col-lg-12">
          <table class="table table-bordered">
            <tr class="cart_prod_head">
              <td class="img_cart">Товар</td>
              <td class="title_cart">Описание</td>
              <td class="price_cart">Цена за единицу</td>
              <td class="value_cart">Кол-во</td>
              <td class="rez_price_cart">Стоимость</td>
            </tr>
            <tr class="cart_prod_content">
              <td class="img_cart"><img src="images/prod1.jpg"></td>
              <td class="title_cart">Рюкзак туристический</td>
              <td class="price_cart">3500 руб</td>
              <td class="value_cart">
                <div>
                  <input type="text" value="1">
                  <span>-</span>
                  <span>+</span>
                </div>
              </td>
              <td class="rez_price_cart">3500 руб</td>
            </tr>
            <tr class="cart_prod_footer">
              <td colspan="2" class="null_cart"></td>
              <td colspan="2" class="rez_title_cart">Итого, к оплате:</td>
              <td class="rez_price_cart">3500 руб</td>
            </tr>
          </table>
          </div>
          <div class="col-lg-12 btn_cart_wrap">
            <a href="#" class="btn_cart_im"><i class="glyphicon glyphicon-chevron-left"></i>Продолжить покупки</a>
            <a href="#" class="btn_cart_zakaz">Оформить заказ<i class="glyphicon glyphicon-chevron-right"></i></a>
          </div>
        </div>
      </div>
    </div>